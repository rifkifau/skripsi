# Compare 3D Visualization
Perbandingan visualisasi 3D antara citra anaglyph (tiga macam skala) dengan tampilan hillshade.

## Project Screenshot
![komparasi layers](https://user-images.githubusercontent.com/24805357/36512827-d728af56-179e-11e8-9db0-4ff6190aee28.jpg)

## [View it live](https://rifkifau.github.io/compare-3d-visualization)
