import React from 'react';
import ReactDOM from 'react-dom';
import ol from 'openlayers';
import {IntlProvider} from 'react-intl';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import Button from '@boundlessgeo/sdk/components/Button';
import Header from '@boundlessgeo/sdk/components/Header';
import enMessages from '@boundlessgeo/sdk/locale/en';
import InfoPopup from '@boundlessgeo/sdk/components/InfoPopup';
import MapPanel from '@boundlessgeo/sdk/components/MapPanel';
import {ToolbarGroup, ToolbarSeparator} from 'material-ui/Toolbar';

import ZoomToLatLon from '@boundlessgeo/sdk/components/ZoomToLatLon';
import Geocoding from '@boundlessgeo/sdk/components/Geocoding';
import GeocodingResults from '@boundlessgeo/sdk/components/GeocodingResults';
import FeatureTable from '@boundlessgeo/sdk/components/FeatureTable';
import QueryBuilder from '@boundlessgeo/sdk/components/QueryBuilder';
import LayerList from '@boundlessgeo/sdk/components/LayerList';
import Geolocation from '@boundlessgeo/sdk/components/Geolocation';
import DrawFeature from '@boundlessgeo/sdk/components/DrawFeature';
import EditPopup from '@boundlessgeo/sdk/components/EditPopup';
import Navigation from '@boundlessgeo/sdk/components/Navigation';
import Select from '@boundlessgeo/sdk/components/Select';
import HomeButton from '@boundlessgeo/sdk/components/HomeButton';
import ZoomSlider from '@boundlessgeo/sdk/components/ZoomSlider';
import Bookmarks from '@boundlessgeo/sdk/components/Bookmarks';
import Fullscreen from '@boundlessgeo/sdk/components/Fullscreen';
import Zoom from '@boundlessgeo/sdk/components/Zoom';
import LoadingPanel from '@boundlessgeo/sdk/components/LoadingPanel';
import Measure from '@boundlessgeo/sdk/components/Measure';
import QGISLegend from '@boundlessgeo/sdk/components/QGISLegend';
import Rotate from '@boundlessgeo/sdk/components/Rotate';
import injectTapEventPlugin from 'react-tap-event-plugin';

// Needed for onTouchTap
// Can go away when react 1.0 release
// Check this repo:
// https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

var defaultFill = new ol.style.Fill({
   color: 'rgba(255,255,255,0.4)'
 });
 var defaultStroke = new ol.style.Stroke({
   color: '#3399CC',
   width: 1.25
 });
 var defaultSelectionFill = new ol.style.Fill({
   color: 'rgba(255,255,0,0.4)'
 });
 var defaultSelectionStroke = new ol.style.Stroke({
   color: '#FFFF00',
   width: 1.25
 });


var categories_geologi_area = function(){ return {"Aluvial": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(254,255,238,1.0)"}),
zIndex: 0
                            })
                            ],
"Andesit": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(240,65,104,0.992156862745)"}),
zIndex: 0
                            })
                            ],
"Batuan Gunungapi Tak Terpisahkan": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(252,216,202,1.0)"}),
zIndex: 0
                            })
                            ],
"Diorit": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(195,0,8,1.0)"}),
zIndex: 0
                            })
                            ],
"Endapan Gunungapi Merapi Tua": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(218,90,77,1.0)"}),
zIndex: 0
                            })
                            ],
"Endapan Gunungapi Muda Merapi": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255,217,201,1.0)"}),
zIndex: 0
                            })
                            ],
"Endapan Longsoran (Ladu) dari Awan Panas": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255,111,81,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Jonggrangan": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(132,190,140,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Kebobutak": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255,189,137,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Kepek": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(133,201,222,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Mandalika": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(203,64,45,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Nampol": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(236,238,214,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Nanggulan": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(227,251,201,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Oyo": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255,255,202,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Sambipitu": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(250,243,101,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Semilir": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255,213,122,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Sentolo": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(253,252,222,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Wonosari": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(209,240,255,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Wungkal": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(163,199,125,1.0)"}),
zIndex: 0
                            })
                            ],
"Formasi Wuni": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255,215,97,1.0)"}),
zIndex: 0
                            })
                            ],
"Koluvial": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255,255,195,1.0)"}),
zIndex: 0
                            })
                            ],
"Kubah Lava dan Leleran": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(248,125,145,1.0)"}),
zIndex: 0
                            })
                            ],
"Nglanggran Formation": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(236,165,123,1.0)"}),
zIndex: 0
                            })
                            ],
"": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(72,206,46,1.0)"}),
zIndex: 0
                            })
                            ]};};var categoriesSelected_geologi_area = {"Aluvial": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Andesit": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Batuan Gunungapi Tak Terpisahkan": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Diorit": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Endapan Gunungapi Merapi Tua": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Endapan Gunungapi Muda Merapi": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Endapan Longsoran (Ladu) dari Awan Panas": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Jonggrangan": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Kebobutak": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Kepek": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Mandalika": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Nampol": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Nanggulan": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Oyo": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Sambipitu": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Semilir": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Sentolo": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Wonosari": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Wungkal": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Formasi Wuni": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Koluvial": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Kubah Lava dan Leleran": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"Nglanggran Formation": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ]};
                    var textStyleCache_geologi_area={}
                    var clusterStyleCache_geologi_area={}
                    var style_geologi_area = function(feature, resolution){
                        var context = {
            feature: feature,
            variables: {},
            layer: 'lyr_geologi_area'
        };
                        
                        var value = feature.get("NAME_BI");
                        var style = categories_geologi_area()[value];
                        var allStyles = [];
                        
                        allStyles.push.apply(allStyles, style);
                        return allStyles;
                    };
                    var selectionStyle_geologi_area = function(feature, resolution){
                        var context = {
            feature: feature,
            variables: {},
            layer: 'lyr_geologi_area'
        };
                        var value = feature.get("NAME_BI");
                        var style = categoriesSelected_geologi_area[value]
                        var allStyles = [];
                        
                        allStyles.push.apply(allStyles, style);
                        return allStyles;
                    };
var categories_geologi_line = function(){ return {"Antiklinal": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.6)}),
zIndex: 0
                            })
                            ,new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(22,219,193,1.0)", lineDash: null, width: pixelsFromMm(0.56)}),
zIndex: 5
                            })
                            ],
"Sesar": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.6)}),
zIndex: 0
                            })
                            ,new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(240,66,66,1.0)", lineDash: null, width: pixelsFromMm(0.56)}),
zIndex: 5
                            })
                            ],
"Sesar Geser": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(122,245,0,1.0)", lineDash: null, width: pixelsFromMm(0.6)}),
zIndex: 0
                            })
                            ,new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(85,170,0,1.0)", lineDash: [6], width: pixelsFromMm(0.523077)}),
zIndex: 0
                            })
                            ],
"Sinklinal": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(215,255,35,1.0)", lineDash: null, width: pixelsFromMm(0.6)}),
zIndex: 0
                            })
                            ,new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(85,170,0,1.0)", lineDash: [6], width: pixelsFromMm(0.523077)}),
zIndex: 0
                            })
                            ],
"": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(236,151,91,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
zIndex: 0
                            })
                            ]};};var categoriesSelected_geologi_line = {"Antiklinal": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.6)}),
zIndex: 0
                            })
                            ,new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.56)}),
zIndex: 5
                            })
                            ],
"Sesar": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.6)}),
zIndex: 0
                            })
                            ,new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.56)}),
zIndex: 5
                            })
                            ],
"Sesar Geser": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.6)}),
zIndex: 0
                            })
                            ,new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: [6], width: pixelsFromMm(0.523077)}),
zIndex: 0
                            })
                            ],
"Sinklinal": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.6)}),
zIndex: 0
                            })
                            ,new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: [6], width: pixelsFromMm(0.523077)}),
zIndex: 0
                            })
                            ],
"": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
zIndex: 0
                            })
                            ]};
                    var textStyleCache_geologi_line={}
                    var clusterStyleCache_geologi_line={}
                    var style_geologi_line = function(feature, resolution){
                        var context = {
            feature: feature,
            variables: {},
            layer: 'lyr_geologi_line'
        };
                        
                        var value = feature.get("nama");
                        var style = categories_geologi_line()[value];
                        var allStyles = [];
                        
                        allStyles.push.apply(allStyles, style);
                        return allStyles;
                    };
                    var selectionStyle_geologi_line = function(feature, resolution){
                        var context = {
            feature: feature,
            variables: {},
            layer: 'lyr_geologi_line'
        };
                        var value = feature.get("nama");
                        var style = categoriesSelected_geologi_line[value]
                        var allStyles = [];
                        
                        allStyles.push.apply(allStyles, style);
                        return allStyles;
                    };
var categories_uji_bentuklahan = function(){ return {"1": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(28,225,209,1.0)"}),
zIndex: 0
                            })
                            ],
"2": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(93,220,68,1.0)"}),
zIndex: 0
                            })
                            ],
"3": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(110,110,232,1.0)"}),
zIndex: 0
                            })
                            ],
"4": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(153,41,237,1.0)"}),
zIndex: 0
                            })
                            ],
"5": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(58,136,205,1.0)"}),
zIndex: 0
                            })
                            ],
"6": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(224,180,58,1.0)"}),
zIndex: 0
                            })
                            ],
"7": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(219,92,61,1.0)"}),
zIndex: 0
                            })
                            ],
"8": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(212,14,83,1.0)"}),
zIndex: 0
                            })
                            ],
"9": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(212,95,201,1.0)"}),
zIndex: 0
                            })
                            ],
"10": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(178,203,114,1.0)"}),
zIndex: 0
                            })
                            ],
"": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(112,239,159,1.0)"}),
zIndex: 0
                            })
                            ]};};var categoriesSelected_uji_bentuklahan = {"1": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"2": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"3": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"4": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"5": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"6": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"7": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"8": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"9": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"10": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ],
"": [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ]};
                    var textStyleCache_uji_bentuklahan={}
                    var clusterStyleCache_uji_bentuklahan={}
                    var style_uji_bentuklahan = function(feature, resolution){
                        var context = {
            feature: feature,
            variables: {},
            layer: 'lyr_uji_bentuklahan'
        };
                        
                        var value = feature.get("test_bl");
                        var style = categories_uji_bentuklahan()[value];
                        var allStyles = [];
                        
            var labelContext = {
                feature: feature,
                variables: {},
                layer: 'lyr_uji_bentuklahan'
            };
            if (getFeatureAttribute(feature, "test_bl") !== null) {
                var labelText = String(getFeatureAttribute(feature, "test_bl"));
            } else {
                var labelText = " ";
            }
            var key = value + "_" + labelText + "_" + String(resolution);
            if (!textStyleCache_uji_bentuklahan[key]){
                var size = 8.25 * 2;
                var font = 'normal normal ' + String(size) + 'px "MS Shell Dlg 2",sans-serif'
                var text = new ol.style.Text({
                      font: font,
                      text: labelText,
                      fill: new ol.style.Fill({
                        color: "rgba(0, 0, 0, 255)"
                      }),
                      textBaseline: "middle",
                      textAlign: "center",
                      rotation: -0.0,
                      offsetX: 0.0,
                      offsetY: 0.0 
                    });
                textStyleCache_uji_bentuklahan[key] = new ol.style.Style({zIndex: 1000, text: text});
            }
            allStyles.push(textStyleCache_uji_bentuklahan[key]);
            
                        allStyles.push.apply(allStyles, style);
                        return allStyles;
                    };
                    var selectionStyle_uji_bentuklahan = function(feature, resolution){
                        var context = {
            feature: feature,
            variables: {},
            layer: 'lyr_uji_bentuklahan'
        };
                        var value = feature.get("test_bl");
                        var style = categoriesSelected_uji_bentuklahan[value]
                        var allStyles = [];
                        
            var labelContext = {
                feature: feature,
                variables: {},
                layer: 'lyr_uji_bentuklahan'
            };
            if (getFeatureAttribute(feature, "test_bl") !== null) {
                var labelText = String(getFeatureAttribute(feature, "test_bl"));
            } else {
                var labelText = " ";
            }
            var key = value + "_" + labelText + "_" + String(resolution);
            if (!textStyleCache_uji_bentuklahan[key]){
                var size = 8.25 * 2;
                var font = 'normal normal ' + String(size) + 'px "MS Shell Dlg 2",sans-serif'
                var text = new ol.style.Text({
                      font: font,
                      text: labelText,
                      fill: new ol.style.Fill({
                        color: "rgba(0, 0, 0, 255)"
                      }),
                      textBaseline: "middle",
                      textAlign: "center",
                      rotation: -0.0,
                      offsetX: 0.0,
                      offsetY: 0.0 
                    });
                textStyleCache_uji_bentuklahan[key] = new ol.style.Style({zIndex: 1000, text: text});
            }
            allStyles.push(textStyleCache_uji_bentuklahan[key]);
            
                        allStyles.push.apply(allStyles, style);
                        return allStyles;
                    };

                    var textStyleCache_elevasi_test={}
                    var clusterStyleCache_elevasi_test={}
                    var style_elevasi_test = function(feature, resolution){
                        var context = {
            feature: feature,
            variables: {},
            layer: 'lyr_elevasi_test'
        };
                        
                        var value = "";
                        var style = [ new ol.style.Style({
                                image: new ol.style.Circle({radius: pixelsFromMm(2/ 2.0), stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.0)}), fill: new ol.style.Fill({color: "rgba(67,150,77,1.0)"})}),
zIndex: 0
                            })
                            ];
                        var allStyles = [];
                        
            var labelContext = {
                feature: feature,
                variables: {},
                layer: 'lyr_elevasi_test'
            };
            if (getFeatureAttribute(feature, "test") !== null) {
                var labelText = String(getFeatureAttribute(feature, "test"));
            } else {
                var labelText = " ";
            }
            var key = value + "_" + labelText + "_" + String(resolution);
            if (!textStyleCache_elevasi_test[key]){
                var size = 10.25 * 2;
                var font = 'normal normal ' + String(size) + 'px "MS Shell Dlg 2",sans-serif'
                var text = new ol.style.Text({
                      font: font,
                      text: labelText,
                      fill: new ol.style.Fill({
                        color: "rgba(0, 0, 0, 255)"
                      }),
                      textBaseline: "middle",
                      textAlign: "center",
                      rotation: -0.0,
                      offsetX: 0.0,
                      offsetY: 0.0 
                    });
                textStyleCache_elevasi_test[key] = new ol.style.Style({zIndex: 1000, text: text});
            }
            allStyles.push(textStyleCache_elevasi_test[key]);
            
                        allStyles.push.apply(allStyles, style);
                        return allStyles;
                    };
                    var selectionStyle_elevasi_test = function(feature, resolution){
                        var context = {
            feature: feature,
            variables: {},
            layer: 'lyr_elevasi_test'
        };
                        var value = "";
                        var style = [ new ol.style.Style({
                                image: new ol.style.Circle({radius: pixelsFromMm(2/ 2.0), stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.0)}), fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"})}),
zIndex: 0
                            })
                            ]
                        var allStyles = [];
                        
            var labelContext = {
                feature: feature,
                variables: {},
                layer: 'lyr_elevasi_test'
            };
            if (getFeatureAttribute(feature, "test") !== null) {
                var labelText = String(getFeatureAttribute(feature, "test"));
            } else {
                var labelText = " ";
            }
            var key = value + "_" + labelText + "_" + String(resolution);
            if (!textStyleCache_elevasi_test[key]){
                var size = 10.25 * 2;
                var font = 'normal normal ' + String(size) + 'px "MS Shell Dlg 2",sans-serif'
                var text = new ol.style.Text({
                      font: font,
                      text: labelText,
                      fill: new ol.style.Fill({
                        color: "rgba(0, 0, 0, 255)"
                      }),
                      textBaseline: "middle",
                      textAlign: "center",
                      rotation: -0.0,
                      offsetX: 0.0,
                      offsetY: 0.0 
                    });
                textStyleCache_elevasi_test[key] = new ol.style.Style({zIndex: 1000, text: text});
            }
            allStyles.push(textStyleCache_elevasi_test[key]);
            
                        allStyles.push.apply(allStyles, style);
                        return allStyles;
                    };

                    var textStyleCache_digitasi_bentuklahan={}
                    var clusterStyleCache_digitasi_bentuklahan={}
                    var style_digitasi_bentuklahan = function(feature, resolution){
                        var context = {
            feature: feature,
            variables: {},
            layer: 'lyr_digitasi_bentuklahan'
        };
                        
                        var value = "";
                        var style = [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(190,167,139,1.0)"}),
zIndex: 0
                            })
                            ];
                        var allStyles = [];
                        
                        allStyles.push.apply(allStyles, style);
                        return allStyles;
                    };
                    var selectionStyle_digitasi_bentuklahan = function(feature, resolution){
                        var context = {
            feature: feature,
            variables: {},
            layer: 'lyr_digitasi_bentuklahan'
        };
                        var value = "";
                        var style = [ new ol.style.Style({
                                stroke: new ol.style.Stroke({color: "rgba(255, 204, 0, 1)", lineDash: null, width: pixelsFromMm(0.26)}),
                            fill: new ol.style.Fill({color: "rgba(255, 204, 0, 1)"}),
zIndex: 0
                            })
                            ]
                        var allStyles = [];
                        
                        allStyles.push.apply(allStyles, style);
                        return allStyles;
                    };
var baseLayers = [new ol.layer.Tile({
    type: 'base',
    title: 'Digital Globe imagery',
    source: new ol.source.XYZ({
        attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})],
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdvtggo66fvw2smomzmmx800/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw'
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'Merapi Merbabu VE 3x',
    source: new ol.source.XYZ({
		attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})],
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdjr3tig3a1g2rp9cynue9kv/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw'
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'Hillshade Palsar VE 1x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdemcllv2oct2rs3a3zo0fw1/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'Hillshade Palsar VE 3x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdemdss63taa2rmuidbuktk5/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'Hillshade Palsar VE 5x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdemeusz1yd32rmu7dok537d/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'R-C 30 meter VE 1x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdelisps3si22rmuad8lc1zx/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'R-C 30 meter VE 3x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdelkxj400av2rquff502vit/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'R-C 30 meter VE 5x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdelndts3hzr2rr2eka5te0f/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'R-C 15 meter VE 1x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdem0zy60aa32rnwxi6ogkm2/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'R-C 15 meter VE 3x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdem2alo05zu2rsfcsz26b85/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'R-C 15 meter VE 5x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdem38ds060p2rsfyp284mgc/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'R-C 10 meter VE 1x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdemnbniep0e2rpnuehekd91/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'R-C 10 meter VE 3x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdemp0o0epdp2tokvurwwnhv/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'R-C 10 meter VE 5x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdemrxbdep0n2smd3isz8pwe/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'A-B 30 meter VE 1x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdelpcks0a132sqek8nzekae/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'A-B 30 meter VE 3x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdelqrlc1xqn2rmuiftr631c/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'A-B 30 meter VE 5x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdelso2o1xsc2rmuiatecpb7/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'A-B 15 meter VE 1x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdem8vmu0ai32sobsgxiqunz/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'A-B 15 meter VE 3x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdem9u1qer3q2smnb160anzw/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'A-B 15 meter VE 5x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdematv8csu32ro3j0cqr8qn/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'A-B 10 meter VE 1x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdf5k3lx56652rmldibip8p0/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'A-B 10 meter VE 3x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdf5l0w30sjd2smsafqb1vaf/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'A-B 10 meter VE 5x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdf5lpfj0mdp2spksbvfcfh8/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'G-M 30 meter VE 1x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdelvgq80a502rnwew37xo4r/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'G-M 30 meter VE 3x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdelwy280a8r2smsv5ibdnj6/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'G-M 30 meter VE 5x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdelyajd0a8e2rpk2agrfdsj/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'G-M 15 meter VE 1x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdem5isl0ag62sqe4yqdkrij/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'G-M 15 meter VE 3x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdem6jhy4o3v2rml9tw08hgm/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'G-M 15 meter VE 5x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdem7jkkcstc2sp5icla6bwy/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'G-M 10 meter VE 1x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdemv7wcepeu2rph2icz7ey7/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'G-M 10 meter VE 3x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdemwke7ep8m2rpnj7u7xhpo/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
,new ol.layer.Tile({
    type: 'base',
    title: 'G-M 10 meter VE 5x',
    source: new ol.source.XYZ({
        url: 'https://api.mapbox.com/styles/v1/rifkifau/cjdemxx1fcx2t2spd9gzs00ei/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicmlma2lmYXUiLCJhIjoiY2pkY3B4a3poMzYzbjMzcjJremE2bWs1OCJ9.J9sISIYJq9fmTI76aM4gJw',
attributions: [new ol.Attribution({ html: 'Tiles data &copy; <a href="http://spasialkan.com/">Spasialkan.com</a>, <a href="https://www.mapbox.com/about/maps">Mapbox</a>'})]
    }),
    projection: 'EPSG:3857'
})
];var baseLayersGroup = new ol.layer.Group({showContent: true,
                    'isGroupExpanded': false, 'type': 'base-group',
                    'title': 'Base maps', layers: baseLayers});var overviewMapBaseLayer = baseLayersGroup;
var overlayLayers = [];var overlaysGroup = new ol.layer.Group({showContent: true,
                        'isGroupExpanded': false, 'title': 'Overlays', layers: overlayLayers});
var lyr_geologi_area = new ol.layer.Vector({
                    opacity: 1.0,
                    source: new ol.source.Vector({
                            format: new ol.format.GeoJSON(),
                            url: './data/lyr_geologi_area.json'
                            }),
                     
                    style: style_geologi_area,
                    selectedStyle: selectionStyle_geologi_area,
                    title: "Geologi_Area",
                    id: "Geologi_Area20180210112309112",
                    filters: [],
                    timeInfo: null,
                    isSelectable: true,
                    popupInfo: "<table class='popup-table'><tr><th>Attribute</th><th>Value</th><tr><td>Simbol</td><td style='text-align:right'>[SYMBOLS]</td></tr><tr><td>Formasi</td><td style='text-align:right'>[NAME_BI]</td></tr><tr><td>Lithologi</td><td style='text-align:right'>[CLASS_LITH]</td></tr></table>",
                    attributes: ["SYMBOLS", "NAME_BI", "CLASS_LITH", "T_CLASS_EN", "B_CLASS_EN", "CLASS_TECT", "B_ERA_BI", "B_PERIOD_B", "B_EPOCH_BI", "B_AGE", "B_EVID_BI", "T_ERA_BI", "T_PERIOD_B", "T_EPOCH_ID", "T_EPOCH_BI", "T_AGE", "T_EVID_BI"],
                    geometryType: "Polygon"
                });
var lyr_geologi_area_overview = new ol.layer.Vector({
                    source: new ol.source.Vector({
                            format: new ol.format.GeoJSON(),
                            url: './data/lyr_geologi_area.json'
                            }),
                     
                    style: style_geologi_area});
var lyr_geologi_line = new ol.layer.Vector({
                    opacity: 1.0,
                    source: new ol.source.Vector({
                            format: new ol.format.GeoJSON(),
                            url: './data/lyr_geologi_line.json'
                            }),
                     
                    style: style_geologi_line,
                    selectedStyle: selectionStyle_geologi_line,
                    title: "Geologi_Line",
                    id: "Geologi_Line20180210113754958",
                    filters: [],
                    timeInfo: null,
                    isSelectable: true,
                    popupInfo: "<table class='popup-table'><tr><th>Attribute</th><th>Value</th><tr><td>Struktur</td><td style='text-align:right'>[nama]</td></tr></table>",
                    attributes: ["OBJECTID", "nama", "Shape_Leng"],
                    geometryType: "Line"
                });
var lyr_geologi_line_overview = new ol.layer.Vector({
                    source: new ol.source.Vector({
                            format: new ol.format.GeoJSON(),
                            url: './data/lyr_geologi_line.json'
                            }),
                     
                    style: style_geologi_line});
var lyr_uji_bentuklahan = new ol.layer.Vector({
                    opacity: 0.25,
                    source: new ol.source.Vector({
                            format: new ol.format.GeoJSON(),
                            url: './data/lyr_uji_bentuklahan.json'
                            }),
                     
                    style: style_uji_bentuklahan,
                    selectedStyle: selectionStyle_uji_bentuklahan,
                    title: "Uji_Bentuklahan",
                    id: "Uji_Bentuklahan20180212215426500",
                    filters: [],
                    timeInfo: null,
                    isSelectable: true,
                    popupInfo: "",
                    attributes: ["FID_landfo", "test_bl"],
                    geometryType: "Polygon"
                });
var lyr_uji_bentuklahan_overview = new ol.layer.Vector({
                    source: new ol.source.Vector({
                            format: new ol.format.GeoJSON(),
                            url: './data/lyr_uji_bentuklahan.json'
                            }),
                     
                    style: style_uji_bentuklahan});
var lyr_elevasi_test = new ol.layer.Vector({
                    opacity: 1.0,
                    source: new ol.source.Vector({
                            format: new ol.format.GeoJSON(),
                            url: './data/lyr_elevasi_test.json'
                            }),
                     
                    style: style_elevasi_test,
                    selectedStyle: selectionStyle_elevasi_test,
                    title: "Elevasi_Test",
                    id: "Elevasi_Test20180210112642987",
                    filters: [],
                    timeInfo: null,
                    isSelectable: true,
                    popupInfo: "",
                    attributes: ["id", "test"],
                    geometryType: "Point"
                });
var lyr_elevasi_test_overview = new ol.layer.Vector({
                    source: new ol.source.Vector({
                            format: new ol.format.GeoJSON(),
                            url: './data/lyr_elevasi_test.json'
                            }),
                     
                    style: style_elevasi_test});
var lyr_digitasi_bentuklahan = new ol.layer.Vector({
                    opacity: 1.0,
                    source: new ol.source.Vector({
                            format: new ol.format.GeoJSON(),
                            url: './data/lyr_digitasi_bentuklahan.json'
                            }),
                     
                    style: style_digitasi_bentuklahan,
                    selectedStyle: selectionStyle_digitasi_bentuklahan,
                    title: "Digitasi_Bentuklahan",
                    id: "Digitasi_Bentuklahan20180210112820145",
                    filters: [],
                    timeInfo: null,
                    isSelectable: true,
                    popupInfo: "<table class='popup-table'><tr><th>Attribute</th><th>Value</th><tr><td>Asal Proses</td><td style='text-align:right'>[AsalProses]</td></tr><tr><td>Bentuklahan</td><td style='text-align:right'>[Bentuklaha]</td></tr></table>",
                    attributes: ["id", "AsalProses", "Bentuklaha"],
                    geometryType: "Polygon"
                });
var lyr_digitasi_bentuklahan_overview = new ol.layer.Vector({
                    source: new ol.source.Vector({
                            format: new ol.format.GeoJSON(),
                            url: './data/lyr_digitasi_bentuklahan.json'
                            }),
                     
                    style: style_digitasi_bentuklahan});

lyr_geologi_area.setVisible(true);
lyr_geologi_line.setVisible(true);
lyr_uji_bentuklahan.setVisible(true);
lyr_elevasi_test.setVisible(true);
lyr_digitasi_bentuklahan.setVisible(true);
for (var i=0;i<baseLayers.length;i++){baseLayers[i].setVisible(false);}
baseLayers[0].setVisible(true);
var layersList = [lyr_geologi_area,lyr_geologi_line,lyr_uji_bentuklahan,lyr_elevasi_test,lyr_digitasi_bentuklahan];layersList.unshift(baseLayersGroup);
var layersMap  = {'lyr_geologi_area':lyr_geologi_area,'lyr_geologi_line':lyr_geologi_line,'lyr_uji_bentuklahan':lyr_uji_bentuklahan,'lyr_elevasi_test':lyr_elevasi_test,'lyr_digitasi_bentuklahan':lyr_digitasi_bentuklahan};
var bookmarks = [{"name": "A", "extent": [12291945.406553201, -887324.3500581987, 12291945.406553201, -887324.3500581987], "description": "A"}, {"name": "B", "extent": [12295013.064136285, -888720.0210218506, 12295013.064136285, -888720.0210218506], "description": "B"}, {"name": "C", "extent": [12290691.387726048, -882862.0312228502, 12290691.387726048, -882862.0312228502], "description": "C"}, {"name": "D", "extent": [12289266.1970856, -884517.2766079257, 12289266.1970856, -884517.2766079257], "description": "D"}, {"name": "E", "extent": [12267633.282362306, -860671.0357748036, 12267633.282362306, -860671.0357748036], "description": "E"}, {"name": "F", "extent": [12263973.634101525, -856901.4134384776, 12263973.634101525, -856901.4134384776], "description": "F"}, {"name": "G", "extent": [12276192.793175649, -879226.773480705, 12276192.793175649, -879226.773480705], "description": "G"}, {"name": "H", "extent": [12277073.402321396, -875269.804150398, 12277073.402321396, -875269.804150398], "description": "H"}, {"name": "I", "extent": [12266252.380417066, -875086.4053003605, 12266252.380417066, -875086.4053003605], "description": "I"}, {"name": "J", "extent": [12260869.692112565, -868837.0672894015, 12260869.692112565, -868837.0672894015], "description": "J"}, {"name": "K", "extent": [12300286.779269177, -879466.9024787339, 12300286.779269177, -879466.9024787339], "description": "K"}, {"name": "L", "extent": [12298108.912638273, -881980.7252101105, 12298108.912638273, -881980.7252101105], "description": "L"}, {"name": "M", "extent": [12295285.42373407, -880626.7268516307, 12295285.42373407, -880626.7268516307], "description": "M"}, {"name": "N", "extent": [12310620.869831072, -880537.0822604832, 12310620.869831072, -880537.0822604832], "description": "N"}, {"name": "O", "extent": [12308734.281841084, -876934.3997839201, 12308734.281841084, -876934.3997839201], "description": "O"}, {"name": "P", "extent": [12314930.647354433, -879640.9343703825, 12314930.647354433, -879640.9343703825], "description": "P"}, {"name": "Q", "extent": [12310357.225011723, -879943.4910203936, 12310357.225011723, -879943.4910203936], "description": "Q"}, {"name": "R", "extent": [12317187.242644597, -879029.1254794836, 12317187.242644597, -879029.1254794836], "description": "R"}, {"name": "S", "extent": [12319502.108697992, -881772.6551917735, 12319502.108697992, -881772.6551917735], "description": "S"}, {"name": "T", "extent": [12320032.179134801, -879727.074060036, 12320032.179134801, -879727.074060036], "description": "T"}]
var legendData = {"Uji_Bentuklahan20180212215426500": [{"href": "2_0.png", "title": "1"}, {"href": "2_1.png", "title": "2"}, {"href": "2_2.png", "title": "3"}, {"href": "2_3.png", "title": "4"}, {"href": "2_4.png", "title": "5"}, {"href": "2_5.png", "title": "6"}, {"href": "2_6.png", "title": "7"}, {"href": "2_7.png", "title": "8"}, {"href": "2_8.png", "title": "9"}, {"href": "2_9.png", "title": "10"}, {"href": "2_10.png", "title": ""}], "Geologi_Area20180210112309112": [{"href": "0_0.png", "title": "Aluvial"}, {"href": "0_1.png", "title": "Andesit"}, {"href": "0_2.png", "title": "Batuan Gunungapi Tak Terpisahkan"}, {"href": "0_3.png", "title": "Diorit"}, {"href": "0_4.png", "title": "Endapan Gunungapi Merapi Tua"}, {"href": "0_5.png", "title": "Endapan Gunungapi Muda Merapi"}, {"href": "0_6.png", "title": "Endapan Longsoran (Ladu) dari Awan Panas"}, {"href": "0_7.png", "title": "Formasi Jonggrangan"}, {"href": "0_8.png", "title": "Formasi Kebobutak"}, {"href": "0_9.png", "title": "Formasi Kepek"}, {"href": "0_10.png", "title": "Formasi Mandalika"}, {"href": "0_11.png", "title": "Formasi Nampol"}, {"href": "0_12.png", "title": "Formasi Nanggulan"}, {"href": "0_13.png", "title": "Formasi Oyo"}, {"href": "0_14.png", "title": "Formasi Sambipitu"}, {"href": "0_15.png", "title": "Formasi Semilir"}, {"href": "0_16.png", "title": "Formasi Sentolo"}, {"href": "0_17.png", "title": "Formasi Wonosari"}, {"href": "0_18.png", "title": "Formasi Wungkal"}, {"href": "0_19.png", "title": "Formasi Wuni"}, {"href": "0_20.png", "title": "Koluvial"}, {"href": "0_21.png", "title": "Kubah Lava dan Leleran"}, {"href": "0_22.png", "title": "Nglanggran Formation"}, {"href": "0_23.png", "title": ""}], "Digitasi_Bentuklahan20180210112820145": [{"href": "4_0.png", "title": ""}], "Geologi_Line20180210113754958": [{"href": "1_0.png", "title": "Antiklinal"}, {"href": "1_1.png", "title": "Sesar"}, {"href": "1_2.png", "title": "Sesar Geser"}, {"href": "1_3.png", "title": "Sinklinal"}, {"href": "1_4.png", "title": ""}], "Elevasi_Test20180210112642987": [{"href": "3_0.png", "title": ""}]};
var view = new ol.View({ maxZoom: 18, minZoom: 1, projection: 'EPSG:3857'});
var originalExtent = [12203488.850144, -925525.284758, 12371047.267010, -816909.991381];
var unitsConversion = 1;

var map = new ol.Map({
  layers: layersList,
  view: view,
  controls: [new ol.control.MousePosition({"projection": "EPSG:4326", "undefinedHTML": "&nbsp;", "coordinateFormat": ol.coordinate.createStringXY(4)}),
new ol.control.Attribution({collapsible: false}),
new ol.control.ScaleLine({"minWidth": 64, "units": "metric"})]
});



class BasicApp extends React.Component {
  getChildContext() {
    return {
      muiTheme: getMuiTheme()
    };
  }
  componentDidMount() {
    map.addControl(new ol.control.OverviewMap({collapsed: true, layers: [overviewMapBaseLayer, lyr_geologi_area_overview,lyr_geologi_line_overview,lyr_uji_bentuklahan_overview,lyr_elevasi_test_overview,lyr_digitasi_bentuklahan_overview]}));
  }
  _toggle(el) {
    if (el.style.display === 'block') {
      el.style.display = 'none';
    } else {
      el.style.display = 'block';
    }
  }
  _toggleTable() {
    this._toggle(document.getElementById('table-panel'));
    this.refs.table.getWrappedInstance().setDimensionsOnState();
  }
  _toggleWFST() {
    this._toggle(document.getElementById('wfst'));
  }
  _toggleQuery() {
    this._toggle(document.getElementById('query-panel'));
  }
  _toggleEdit() {
    this._toggle(document.getElementById('edit-tool-panel'));
  }
  _hideAboutPanel(evt) {
    evt.preventDefault();
    document.getElementById('about-panel').style.display = 'none';
  }
  _toggleChartPanel(evt) {
    evt.preventDefault();
    this._toggle(document.getElementById('chart-panel'));
  }
  render() {
    var toolbarOptions = {title:"Citra Anaglyph Multiskala Prov. DIY"};
    return React.createElement("article", null,
      React.createElement(Header, toolbarOptions ,
React.createElement(ZoomToLatLon, {map:map}),
React.createElement("div", {id:'geocoding'},
                                        React.createElement(Geocoding, {maxResults:5})),
React.createElement(Button, {buttonType: 'Icon', iconClassName: 'headerIcons ms ms-table', tooltip: 'Table', onTouchTap: this._toggleTable.bind(this)}),
React.createElement(Button, {buttonType: 'Icon', iconClassName: 'headerIcons fa fa-filter', tooltip: 'Query', onTouchTap: this._toggleQuery.bind(this)}),
React.createElement(IconMenu, {anchorOrigin: {horizontal: 'right', vertical: 'bottom'}, targetOrigin: {horizontal: 'right', vertical: 'top'}, iconButtonElement: React.createElement(Button, {buttonType: 'Icon', iconClassName: "headerIcons ms ms-link", tooltip: "Links"})},
                                        React.createElement(MenuItem, {primaryText: "Uji Interpretabilitas", href:"https://docs.google.com/forms/d/e/1FAIpQLSdiMxXnNL17JxA2NGVZTToG-bEnK276FHvkKC9MS8FyycnRtw/viewform"}),
React.createElement(MenuItem, {primaryText: "Anaglyph WMTS", href:"https://github.com/rifkifau/anaglyph-imagery-diy-wmts"}),
React.createElement(MenuItem, {primaryText: "Anaglyph vs Hillshade", href:"komparasi-layers"}),
React.createElement(MenuItem, {primaryText: "3D Maps VE 1x", href:"animasi-3d/ve1.html"}),
React.createElement(MenuItem, {primaryText: "3D Maps VE 3x", href:"animasi-3d/ve3.html"}),
React.createElement(MenuItem, {primaryText: "3D Maps VE 5x", href:"animasi-3d/ve5.html"})
                                    ),
React.createElement(DrawFeature, {toggleGroup: 'navigation', map: map}),
React.createElement(Navigation, {toggleGroup: 'navigation', secondary: true}),
React.createElement(Select, {toggleGroup: 'navigation', map:map}),
React.createElement(Bookmarks, {introTitle:'', introDescription:'', dots:true,
                                            animatePanZoom:false, menu: true, map: map, bookmarks: bookmarks, width:'400px' })
                                      ,
React.createElement(Button, {buttonType: 'Icon', iconClassName: 'headerIcons ms ms-help', tooltip: 'Help', onTouchTap: function(){window.open('help','_blank')}}),
React.createElement(Measure, {toggleGroup:'navigation', map:map, geodesic:true})),
      React.createElement("div", {id: 'content'},
        React.createElement(MapPanel, {id: 'map', map: map, extent: originalExtent, useHistory: true}
          ,
React.createElement("div", {id: 'about-panel', className:'about-panel'},
                                        React.createElement("a", {href:'#', id:'about-panel-closer',
                                            className:'about-panel-closer', onClick:this._hideAboutPanel.bind(this)},
                                              "X"
                                        ),
                                        React.createElement("div", {dangerouslySetInnerHTML:{__html: '<h1>Anaglyph Imagery</h1><br><p>Woods dan Rourke (2004) menjelaskan bahwa konsep anaglyph adalah menggabungkan dua perspektif gambar menjadi satu gambar menggunakan teknik kode warna.</p><br><p>Misalnya, jika metode anaglyph merah-cyan digunakan, gambar perspektif kiri disimpan dalam saluran merah dan gambar perspektif yang kanan disimpan dalam saluran warna biru dan hijau (biru + hijau = cyan).</p><br><p>Pengamat harus menggunakan kacamata anaglyph berfilter warna sesuai dengan yang digunakan pada citra (Red-Cyan/Green-Magenta/Amber-Blue) untuk mendapatkan kesan tiga dimensi. </p><br><img src="https://spasialkan.com/wp-content/uploads/2017/10/Citra-Visualisasi-Anaglip-Anaglyph-Imagery-Spasialkancom.png" alt="Citra Visualisasi Anaglip - Anaglyph Imagery - Spasialkancom" width="100%"/><br>'}})
                                    ),
React.createElement("div", {id:'geocoding-results', className:'geocoding-results-panel'},
                                    React.createElement(GeocodingResults, {map:map, zoom:10})
                                  ),
React.createElement("div", {id: 'query-panel', className:'query-panel'},
                                          React.createElement(QueryBuilder, {map: map})
                                        ),
React.createElement("div", {id: 'popup', className: 'ol-popup'},
                                    React.createElement(InfoPopup, {toggleGroup: 'navigation', map: map, hover: true})
                                  )
        )
        ,
 React.createElement("div", {id: 'table-panel', className: 'attributes-table'},
                                          React.createElement(FeatureTable, {allowEdit:true, toggleGroup: 'navigation',
                                                              ref: 'table', pointZoom:16, map: map,
                                                              sortable:true, pageSize:20})
                                    ),
React.createElement("div",{id: "layerlist"},
                                    React.createElement(LayerList, {showOpacity:true, showDownload:true,
                                        showZoomTo:true, allowReordering:true,
                                        allowFiltering:true, tipLabel:'layers',
                                        downloadFormat:'GeoJSON', showUpload:true, map:map,
                                        includeLegend:true, allowStyling:true, showTable:true})),
React.createElement("div", {id:'geolocation-control'},
                                    React.createElement(Geolocation, {tooltipPosition: 'bottom-right', map:map})
                                  ),
React.createElement("div", {id: 'editpopup', className: 'ol-popup'},
                                React.createElement(EditPopup, {toggleGroup: 'navigation', map: map})
                            ),
React.createElement("div", {id:'home-button'},
                                    React.createElement(HomeButton, {tooltipPosition: 'bottom-right', map:map})
                                  ),
React.createElement("div", {id:'zoom-slider'},
                                    React.createElement(ZoomSlider, {map:map, refreshRate:100})
                                  ),
React.createElement("div", {id:'fullscreen-button'},
                                    React.createElement(Fullscreen, {tooltipPosition: 'bottom-right', map:map})
                                  ),
React.createElement("div", {id:'zoom-buttons'},
                                    React.createElement(Zoom, {
                                    duration:250,
                                    zoomInTipLabel: 'Zoom in',
                                    zoomOutTipLabel: 'Zoom out',
                                    delta: 1.2,
                                    map: map,
                                    tooltipPosition: 'bottom-right'})
                                  ),
React.createElement(LoadingPanel, {map:map}),
React.createElement("div",{id: "legend"},
                                React.createElement(QGISLegend, {map:map, size:20, legendBasePath:'./resources/legend/',showExpandedOnStartup:false, legendData:legendData})
                            ),
React.createElement("div", {id:'rotate-button'},
                                    React.createElement(Rotate, {
                                    autoHide:false,
                                    duration:250,
                                    map: map,
                                    tooltipPosition: 'bottom-right'})
                                  )
      )
    );
  }
}

BasicApp.childContextTypes = {
  muiTheme: React.PropTypes.object
};

ReactDOM.render(<IntlProvider locale='en' messages={enMessages}><BasicApp /></IntlProvider>, document.getElementById('main'));
