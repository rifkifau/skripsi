# Project Skripsi
Citra Anaglyph Multiskala Berbasis WebGIS untuk Membantu Interpretasi Bentuklahan On-Screen

## Project Screenshot
![skripsi](https://user-images.githubusercontent.com/24805357/36516832-54b22930-17b2-11e8-890f-02eccb02673f.jpg)

## View it live:
<b>[WebGIS](https://rifkifau.github.io/skripsi)</b>

## Newest: Padlock, solved.
Hijau itu menyejukkan.

## Need Help!
Amati dan uji visualisasi anaglyph dalam merepresentasikan permukaan bumi secara 3 dimensi.
Di sini link formnya: <b>[link.spasialkan.com/ujiskripsi](http://link.spasialkan.com/ujiskripsi)</b>
